package server_test

import (
	"fmt"
	"strings"
	"sync"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal/server"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal/templater"
)

func TestProcessor(t *testing.T) {
	// logrus.SetLevel(logrus.DebugLevel)
	logrus.SetLevel(logrus.InfoLevel)

	sender := &mockSender{}
	pc := server.NewMatchProcessor(
		templater.NewTemplater(),
		sender,
	)

	wg := &sync.WaitGroup{}
	wg.Add(3)

	ch := make(chan server.MatchPayload, 1)
	go func() {
		pc.Do(ch)
		wg.Done() // Last one, when we finish processing the channel after closing
	}()

	runner := mockRunner{wg: wg}
	runner2 := mockRunner{wg: wg, err: fmt.Errorf("random crazy error")}

	ch <- server.MatchPayload{
		AlertGroup: internal.AlertGroup{
			Version: "4",
			Status:  "firing",
			Alerts: internal.Alerts{
				internal.Alert{Annotations: map[string]string{
					"my-label": "my-value",
				}},
			},
		},
		Match: runner,
	}

	ch <- server.MatchPayload{
		AlertGroup: internal.AlertGroup{
			Version: "4",
			Status:  "firing",
			Alerts: internal.Alerts{
				internal.Alert{Annotations: map[string]string{
					"my-label": "my-second-value",
				}},
			},
		},
		Match: runner2,
	}

	close(ch)

	runner.wg.Wait() // Wait for full flushing

	require.Equal(t, "match - matched mock\n"+
		"success - successful mock: ```done execution```\n"+
		"match - matched mock\n"+
		"failure - failure mock: random crazy error\n\n"+
		"```output for failed execution```", sender.sentMessage)
}

type mockRunner struct {
	wg *sync.WaitGroup

	err error
}

func (mockRunner) Name() string {
	return "mock"
}

func (mockRunner) Template() *internal.MessageTemplate {
	return nil
}

func (mr mockRunner) Execute() (string, error) {
	defer mr.wg.Done()

	if mr.err != nil {
		return "output for failed execution", mr.err
	}

	return "done execution", nil
}

type mockSender struct {
	sentMessage string
}

func (m *mockSender) Send(event internal.Event, message string) error {
	m.sentMessage = strings.TrimSpace(fmt.Sprintf("%s\n%s - %s", m.sentMessage, event, message))
	return nil
}
