#!/bin/bash

set -e

export TAG="${1:-latest}"
export CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE:-chief-alert-executor}"

for ARCH in amd64 arm64 arm; do
    echo "Building ${CI_REGISTRY_IMAGE}:${TAG}-${ARCH}"
    docker build \
        --cache-from ${CI_REGISTRY_IMAGE}:latest \
        --build-arg arch=${ARCH} \
        --tag ${CI_REGISTRY_IMAGE}:${TAG}-${ARCH} .

    docker push ${CI_REGISTRY_IMAGE}:${TAG}-${ARCH}
done

echo "Building multiarch manifest for ${CI_REGISTRY_IMAGE}:${TAG}"
docker manifest create ${CI_REGISTRY_IMAGE}:${TAG} \
    --amend ${CI_REGISTRY_IMAGE}:${TAG}-amd64 \
    --amend ${CI_REGISTRY_IMAGE}:${TAG}-arm64 \
    --amend ${CI_REGISTRY_IMAGE}:${TAG}-arm

echo "Pushing multiarch manifest ${CI_REGISTRY_IMAGE}:${TAG}"
docker manifest push ${CI_REGISTRY_IMAGE}:${TAG}