package server

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal/matcher"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal/templater"
)

func NewMatchProcessor(templater templater.Templater, messenger internal.Messenger) MatchProcessor {
	return MatchProcessor{
		templater: templater,
		messenger: messenger,
	}
}

type MatchProcessor struct {
	templater templater.Templater
	messenger internal.Messenger
}

func (mp MatchProcessor) Do(matches chan MatchPayload) {
	log.Println("starting matches processor")

	for m := range matches {
		templater := mp.templater.WithTemplate(m.Match.Template())
		logger := log.WithField("templater", templater).
			WithField("payload", fmt.Sprintf("%#v", m.AlertGroup)).
			WithField("match", fmt.Sprintf("%#v", m.Match))

		event := internal.MatchEvent
		message, err := templater.Expand(internal.MatchEvent, m)

		if err != nil {
			logger.WithField("event", "match").
				Errorf("failed to expand template: %s", err)
		} else {
			err = mp.messenger.Send(event, message)
			if err != nil {
				logger.WithField("event", "match").
					WithField("message", message).
					Warnf("failed to send message: %s", err)
			}
		}

		output, err := m.Match.Execute()
		payload := struct {
			AlertGroup internal.AlertGroup
			Match      matcher.Runner
			Output     string
			Err        error
		}{
			m.AlertGroup,
			m.Match,
			output,
			err,
		}

		logger = logger.WithField("payload-with-output", fmt.Sprintf("%#v", payload))
		if err == nil {
			event = internal.SuccessEvent
			message, err = templater.Expand(internal.SuccessEvent, payload)
			if err != nil {
				logger.WithField("event", "success").
					Warnf("failed to expand message: %s", err)
			}
		} else {
			event = internal.FailureEvent
			message, err = templater.Expand(internal.FailureEvent, payload)
			if err != nil {
				logger.WithField("event", "failure").
					Warnf("failed to expand message: %s", err)
			}
		}

		if err = mp.messenger.Send(event, message); err != nil {
			logger.WithField("message", message).
				Errorf("failed to send message: %s", err)
		}
	}
	log.Println("Matches channel closed, ending this instance of matching")
}
