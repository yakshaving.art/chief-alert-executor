package messenger_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/yakshaving.art/chief-alert-executor/internal"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal/messenger"
)

func TestNoopMessenger(t *testing.T) {
	noop := messenger.Noop()
	noop.Send(internal.MatchEvent, "match!")
}

func TestSlackMessenger(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Do nothing
	}))
	defer server.Close()

	slack := messenger.Slack(server.URL)
	slack.Send(internal.MatchEvent, "another match!")

}
