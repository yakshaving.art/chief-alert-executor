FROM alpine:latest

ARG arch

# hadolint ignore=DL3018
RUN apk --no-cache add libc6-compat openssh

COPY chief-alert-executor-$arch /chief-alert-executor

EXPOSE 9099
 
ENTRYPOINT [ "/chief-alert-executor" ]
