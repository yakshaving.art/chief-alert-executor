package templater_test

import (
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/yakshaving.art/chief-alert-executor/internal"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal/templater"
)

func TestTemplater(t *testing.T) {
	logrus.SetLevel(logrus.DebugLevel)
	t.Cleanup(func() { logrus.SetLevel(logrus.InfoLevel) })

	tt := []struct {
		name      string
		templater templater.Templater
		specific  *internal.MessageTemplate
		event     internal.Event
		payload   interface{}
		expected  string
	}{
		{
			"onMatch",
			templater.NewTemplater(),
			nil,
			internal.MatchEvent,
			"payload",
			"debugging matched \"payload\"",
		},
		{
			"OnSuccess",
			templater.NewTemplater(),
			nil,
			internal.SuccessEvent,
			"payload",
			"debugging successful \"payload\"",
		},
		{
			"OnFailure",
			templater.NewTemplater(),
			nil,
			internal.FailureEvent,
			"payload",
			"debugging failure \"payload\"",
		},
		{
			"OnSpecific",
			templater.NewTemplater().WithTemplate(
				&internal.MessageTemplate{
					OnFailure: "specific failure {{ . }}",
				}),
			nil,
			internal.FailureEvent,
			"payload",
			"specific failure payload",
		},
		{
			"OnNoTemplate",
			templater.NewTemplater().WithTemplate(
				&internal.MessageTemplate{
					OnMatch: "",
				},
			),
			nil,
			internal.MatchEvent,
			"payload",
			"debugging matched \"payload\"",
		},
		{
			"OnSillyTemplate",
			templater.NewTemplater().WithTemplate(
				&internal.MessageTemplate{
					OnMatch: "silly",
				},
			),
			nil,
			internal.MatchEvent,
			"payload",
			"silly",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			a := assert.New(t)
			s, err := tc.templater.Expand(tc.event, tc.payload)
			a.NoError(err)
			a.Equal(tc.expected, s)
		})
	}

}
