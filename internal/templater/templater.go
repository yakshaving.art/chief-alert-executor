package templater

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	log "github.com/sirupsen/logrus"

	"gitlab.com/yakshaving.art/chief-alert-executor/internal"
)

func NewTemplater() Templater {
	if log.IsLevelEnabled(log.DebugLevel) {
		return Templater{
			DefaultTemplate: &internal.MessageTemplate{
				OnMatch:   "debugging matched {{ expand . }}",
				OnSuccess: "debugging successful {{ expand . }}",
				OnFailure: "debugging failure {{ expand . }}",
			},
			template: nil,
		}
	}
	return Templater{
		DefaultTemplate: &internal.MessageTemplate{
			OnMatch:   "matched {{ .Match.Name }}",
			OnSuccess: "successful {{ .Match.Name }}: ```{{ .Output }}```",
			OnFailure: "failure {{ .Match.Name }}: {{ .Err }}{{ if .Output }}\n\n```{{ .Output }}```{{ end }}",
		},
		template: nil,
	}
}

// Templater is the object used to handle default and different templates
// depending on the specific matcher configuration
type Templater struct {
	DefaultTemplate *internal.MessageTemplate
	template        *internal.MessageTemplate
}

// WithTemplate sets a specific template and returns a new matcher
func (m Templater) WithTemplate(template *internal.MessageTemplate) Templater {
	m.template = template
	return m
}

// Expand send the message by expanding the template to then send the
// actual message
func (m Templater) Expand(forEvent internal.Event, payload interface{}) (string, error) {
	var msg string

	for i, tx := range []*internal.MessageTemplate{m.template, m.DefaultTemplate} {
		if tx == nil || tx.IsEmpty() {
			log.Debugf("Skipping templater %d because it's nil or empty", i)
			continue
		}

		msg = strings.TrimSpace(tx.GetMessage(forEvent))
		if msg != "" {
			break
		}
	}

	// If there's no template defined, then there's no message to send
	if msg == "" {
		log.Debugf("no template defined for event %s and payload %#v", forEvent, payload)
		return "", nil
	}

	tmpl, err := template.New(string(forEvent)).Funcs(template.FuncMap{
		"expand": func(s interface{}) string { return fmt.Sprintf("%#v", s) },
	}).Parse(msg)
	if err != nil {
		return "", fmt.Errorf("failed to parse template %s for event %s: %s", msg, forEvent, err)
	}

	b := bytes.NewBufferString("")
	err = tmpl.Execute(b, payload)
	if err != nil {
		return "", fmt.Errorf("failed to expand template %s for event %s: %s", msg, forEvent, err)
	}

	return b.String(), nil
}
