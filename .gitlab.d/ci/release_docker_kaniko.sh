#!/bin/bash

set -e

export TAG="${1:-latest}"
export CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE:-chief-alert-executor}"
export CI_PROJECT_DIR="${CI_PROJECT_DIR:-$(pwd)}"

for ARCH in amd64 arm64 arm; do
    echo "Building ${CI_REGISTRY_IMAGE}:${TAG}-${ARCH}"
    /kaniko/executor \
        --build-arg arch=${ARCH} \
        --custom-platform linux/$ARCH \
        --context "${CI_PROJECT_DIR}" \
        --dockerfile "${CI_PROJECT_DIR}/Dockerfile" \
        --destination "${CI_REGISTRY_IMAGE}:${TAG}-${ARCH}"
done

echo "Building multiarch manifest for ${CI_REGISTRY_IMAGE}:${TAG}"
/bin/manifest-tool \
    --docker-cfg /kaniko/.docker/config.json \
    push from-args \
        --platforms linux/amd64,linux/arm64,linux/arm \
        --template ${CI_REGISTRY_IMAGE}:${TAG}-ARCH \
        --target ${CI_REGISTRY_IMAGE}:${TAG}
