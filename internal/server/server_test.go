package server_test

import (
	"sync"
	"testing"

	"gitlab.com/yakshaving.art/chief-alert-executor/internal/messenger"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal/server"
)

func TestServer(t *testing.T) {
	ss := server.New(server.Args{
		Address:        "localhost:31567",
		ConfigFilename: "./fixtures/valid-config.yaml",
		Messenger:      messenger.Noop(),
		Concurrency:    1,
		MetricsPath:    "/metrics",
	})
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		wg.Done()
		ss.Start() // This one locks forever
	}()

	wg.Wait()
	ss.Stop()
}
