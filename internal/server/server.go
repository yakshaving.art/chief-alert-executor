package server

import (
	"fmt"
	"io"
	"net/http"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/yakshaving.art/chief-alert-executor/internal"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal/matcher"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal/metrics"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal/templater"
	"gitlab.com/yakshaving.art/chief-alert-executor/internal/webhook"
)

// SupportedWebhookVersion is the alert webhook data version that is supported
// by this app
const SupportedWebhookVersion = "4"

// Args are the arguments for building a new server
type Args struct {
	MetricsPath    string
	Address        string
	ConfigFilename string

	Messenger   internal.Messenger
	Concurrency int
}

type MatchPayload struct {
	AlertGroup internal.AlertGroup
	Match      matcher.Runner
}

// Server represents a web server that processes webhooks
type Server struct {
	r *mux.Router

	address        string
	alertMatcher   matcher.Matcher
	matchProcessor MatchProcessor

	matches chan MatchPayload
}

// New returns a new web server, or fails misserably
func New(args Args) *Server {
	r := mux.NewRouter()

	log.Debugf("Creating new server with args: %#v", args)

	c, err := Load(args.ConfigFilename)
	if err != nil {
		log.Fatalf("Could not load the configuration file %s: %s", args.ConfigFilename, err)
	}
	alertMatcher, err := matcher.New(c)
	if err != nil {
		log.Fatalf("Could not build an alert matcher with the current configuration: %s", err)
	}

	t := templater.NewTemplater()
	if c.DefaultTemplate != nil && !c.DefaultTemplate.IsEmpty() {
		t.DefaultTemplate = c.DefaultTemplate
	}

	s := &Server{
		r: r,

		address: args.Address,

		alertMatcher: alertMatcher,
		matchProcessor: MatchProcessor{
			templater: t,
			messenger: args.Messenger,
		},

		matches: make(chan MatchPayload, args.Concurrency),
	}

	r.Handle(args.MetricsPath, promhttp.Handler())
	r.HandleFunc("/webhook", s.webhookPost).Methods("POST")
	r.HandleFunc("/-/health", s.healthyProbe).Methods("GET")

	return s
}

// Start starts a new server on the given address
func (s *Server) Start() {
	go func() {
		s.matchProcessor.Do(s.matches)
		log.Println("Channel closed, quitting")
	}()

	log.Println("Starting listener on", s.address)
	log.Fatal(http.ListenAndServe(s.address, s.r))
}

func (s *Server) Stop() {
	log.Println("Closing channel to stop processing")
	close(s.matches)
}

func (s *Server) webhookPost(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	metrics.WebhooksReceivedTotal.Inc()

	body, err := io.ReadAll(r.Body)
	if err != nil {
		metrics.InvalidWebhooksTotal.Inc()
		log.Printf("Failed to read payload: %s\n", err)
		http.Error(w, fmt.Sprintf("Failed to read payload: %s", err), http.StatusBadRequest)
		return
	}

	log.Debugln("Received webhook payload", string(body))

	alertGroup, err := webhook.Parse(body)
	if err != nil {
		metrics.InvalidWebhooksTotal.Inc()

		log.Printf("Invalid payload: %s\n", err)
		http.Error(w, fmt.Sprintf("Invalid payload: %s", err), http.StatusBadRequest)
		return
	}

	if alertGroup.Version != SupportedWebhookVersion {
		metrics.InvalidWebhooksTotal.Inc()

		log.Printf("Invalid payload: webhook version %s is not supported\n", alertGroup.Version)
		http.Error(w, fmt.Sprintf("Invalid payload: webhook version %s is not supported",
			alertGroup.Version), http.StatusBadRequest)
		return
	}

	metrics.AlertsReceivedTotal.Inc()

	match := s.alertMatcher.Match(*alertGroup)
	if match == nil {
		return
	}

	s.matches <- MatchPayload{
		*alertGroup,
		match,
	}
}

func (s *Server) healthyProbe(w http.ResponseWriter, r *http.Request) {
}
