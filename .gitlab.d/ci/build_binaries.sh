#!/bin/bash

set -ex

export GOOS="${GOOS:-linux}"
export VERSION="${VERSION:-dvlpr}"
export COMMIT_ID="${COMMIT_ID:-$(git log --format="%h" -n 1)}"
export COMMIT_DATE="${COMMIT_DATE:-$(date +'%Y-%m-%dT%H:%M:%S%z')}"

export LDFLAGS="-X gitlab.com/yakshaving.art/chief-alert-executor/version.Version=${VERSION} \
                -X gitlab.com/yakshaving.art/chief-alert-executor/version.Commit=${COMMIT_ID} \
                -X gitlab.com/yakshaving.art/chief-alert-executor/version.Date=${COMMIT_DATE}"

for arch in amd64 arm64 arm; do
    output="chief-alert-executor-${arch}"

    echo "Building ${output}"
	env GOARCH=$arch GOOS=linux go build -ldflags "${LDFLAGS}" -o "${output}"
done